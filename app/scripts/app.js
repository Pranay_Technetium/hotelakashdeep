'use strict';

/**
 * @ngdoc overview
 * @name mezbaancaterersApp
 * @description
 * # mezbaancaterersApp
 *
 * Main module of the application.
 */
angular
  .module('mezbaancaterersApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch',
    'ui.router',
    'ngSwipebox',
    'duScroll',
    'mezbaancaterersApp.controllers'
  ])
    // Defination of Url Route Provider
.config(['$stateProvider', '$urlRouterProvider','$locationProvider','$uiViewScrollProvider',function($stateProvider, $urlRouterProvider,$locationProvider,$uiViewScrollProvider) {

  $uiViewScrollProvider.useAnchorScroll();

  $stateProvider

  
  .state('app', {
    url: '/app',
    abstract: true,
    templateUrl: 'views/navigation.html',
    
  })



  .state('app.home', {
    url: '/home',
    templateUrl: 'views/home.html',
    controller: 'HomeCtrl'
  })

  .state('app.about', {
    url: '/about',
    templateUrl: 'views/about.html'
    
  })

  .state('app.gallery', { 
    url: '/gallery',
    templateUrl: 'views/gallery.html',
    controller:'GalleryCtrl'
  })

  
  .state('app.contact', {
    url: '/contact',
    templateUrl: 'views/contact.html'
    
  })

  .state('app.menuindex', {
    url: '/menuindex',
    templateUrl: 'views/menuindex.html'
  })

  .state('app.menu', {
    url: '/menu',
    templateUrl: 'views/menu.html',
    controller: 'MenuCtrl'
    
  });


  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/app/home');
  $locationProvider.hashPrefix('!');
}])


    


.run(['$rootScope','$location','$stateParams', '$anchorScroll',function($rootScope,$location,$stateParams, $anchorScroll) {
    
    $rootScope.$on('$stateChangeSuccess', function() {
    document.body.scrollTop = 0;
    document.documentElement.scrollTop = 0;
});

   
}]);