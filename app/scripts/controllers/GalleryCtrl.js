'use strict';

/**
 * @ngdoc function
 * @name mezbaancaterersApp.controller:mezbaancaterersCtrl
 * @description
 * # AboutCtrl
 * Controller of the mezbaancaterersApp
 */
angular.module('mezbaancaterersApp')
  .controller('GalleryCtrl', function ($scope) {
  	console.log('GalleryCtrl init');
    $scope.photos=[
        {
          id:"1",href:"images/gallery/mezbaan.jpg"
        },
        {
          id:"2",href:"images/gallery/mezbaan10.jpg"
        },
        {
          id:"3",href:"images/gallery/mezbaan11.jpg"
        },
        {
          id:"4",href:"images/gallery/mezbaan12.jpg"
        },
        {
          id:"5",href:"images/gallery/mezbaan13.jpg"
        },
        {
          id:"6",href:"images/gallery/mezbaan14.jpg"
        },
        {
          id:"7",href:"images/gallery/mezbaan15.jpg"
        },
        {
          id:"8",href:"images/gallery/mezbaan16.jpg"
        },
        {
          id:"9",href:"images/gallery/mezbaan17.jpg"
        },
        {
          id:"10",href:"images/gallery/mezbaan2.jpg"
        },
        {
          id:"11",href:"images/gallery/mezbaan20.jpg"
        },
        {
          id:"12",href:"images/gallery/mezbaan22.jpg"
        },
        {
          id:"13",href:"images/gallery/mezbaan23.jpg"
        },
        {
          id:"14",href:"images/gallery/mezbaan27.jpg"
        },
        {
          id:"15",href:"images/gallery/mezbaan3.jpg"
        },  
        {
          id:"16",href:"images/gallery/mezbaan30.jpg"
        }
    ];


});