'use strict';

/**
 * @ngdoc function
 * @name mezbaancaterersApp.controller:mezbaancaterersCtrl
 * @description
 * # HomeBannerCtrl
 * Controller of the mezbaancaterersApp
 */
angular.module('mezbaancaterersApp')
  .controller('HomeBannerCtrl',['$scope','$http',function ($scope,$http) {
  	console.log('HomeBannerCtrl init');
    $scope.slides= [];
    $scope.slides=[
        {
          id:"1",image:"images/banner/banner.jpg"
        },
        {
          id:"2",image:"images/banner/banner2.jpg"
        },
        {
          id:"3",image:"images/banner/banner3.jpg"
        },
        {
          id:"4",image:"images/banner/banner4.jpg"
        },
        {
          id:"5",image:"images/banner/banner5.jpg"
        }
    ];
    console.log($scope.slides);

}]);