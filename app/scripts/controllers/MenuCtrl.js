'use strict';

angular.module('mezbaancaterersApp')
  .controller('MenuCtrl',function ($scope) {
    console.log("MenuCtrl Init");
    $scope.menus=null;
    $scope.menus=[
    	{
	    	"cid":1,
	    	"cname":"Beverages",
	    	"items":
		    	[{
		    		"itemId":11,
		    		"itemName":"Mocktails",
		    		"subItems":[{
		    			"subItemId":111,
		    			"subItemName":'Blue Velvet'
		    		},
		    		{
		    			"subItemId":112,
		    			"subItemName":'Rose Marry'
		    		},
		    		{
		    			"subItemId":113,
		    			"subItemName":'Green Eye'
		    		},
		    		{
		    			"subItemId":114,
		    			"subItemName":'Strawberry Delight'
		    		},
		    		{
		    			"subItemId":115,
		    			"subItemName":'Fruit Punch'
		    		}]

		    	},
		    	{
		    		"itemId":12,
		    		"itemName":"Shakes & Fresh Juices",
		    		"subItems":[{
		    			"subItemId":121,
		    			"subItemName":'Vanilla'
		    		},
		    		{
		    			"subItemId":122,
		    			"subItemName":'Mango'
		    		},
		    		{
		    			"subItemId":123,
		    			"subItemName":'Strawberry'
		    		},
		    		{
		    			"subItemId":124,
		    			"subItemName":'Orange Juice'
		    		},
		    		{
		    			"subItemId":125,
		    			"subItemName":'Mix Fruit Juice'
		    		}]
		    	},
		    	{
		    		"itemId":13,
		    		"itemName":"Soft Drinks",
		    		"subItems":[{
		    			"subItemId":131,
		    			"subItemName":'Cola'
		    		},
		    		{
		    			"subItemId":132,
		    			"subItemName":'Lemon'
		    		},
		    		{
		    			"subItemId":133,
		    			"subItemName":'Orange'
		    		},
		    		{
		    			"subItemId":134,
		    			"subItemName":'Dew'
		    		}]
		    	},
		    	{
		    		"itemId":14,
		    		"itemName":"Water / Soda",
		    		"subItems":[{
		    			"subItemId":141,
		    			"subItemName":'Water Bottles (200 ml.)'
		    		},
		    		{
		    			"subItemId":142,
		    			"subItemName":'Fresh Lime Water / Soda'
		    		}]
		    	},
		    	{
		    		"itemId":15,
		    		"itemName":"Coffee",
		    		"subItems":[{
		    			"subItemId":151,
		    			"subItemName":'Espresso Coffee'
		    		}]
		    	}]
    	},
    	{
    		"cid":2,
   	  		"cname":"Welcome Snacks",
   	  		"items":
		    	[{
		    		"itemId":22,
		    		"itemName":"Paneer Snacks",
		    		"subItems":[{
		    			"subItemId":221,
		    			"subItemName":'Paneer Achari Tikka'
		    		},
		    		{
		    			"subItemId":222,
		    			"subItemName":'Paneer Malai Tikka'
		    		},
		    		{
		    			"subItemId":223,
		    			"subItemName":'Chilli Paneer (Dry)'
		    		},
		    		{
		    			"subItemId":224,
		    			"subItemName":'Paneer Finger'
		    		},
		    		{
		    			"subItemId":225,
		    			"subItemName":'Paneer Haryali Tikka'
		    		},
		    		{
		    			"subItemId":226,
		    			"subItemName":'Paneer Shasliki Tikka'
		    		},
		    		{
		    			"subItemId":227,
		    			"subItemName":'Paneer Adraki Tikka'
		    		},
		    		{
		    			"subItemId":228,
		    			"subItemName":'Paneer Ajwaini Tikka'
		    		},
		    		{
		    			"subItemId":229,
		    			"subItemName":'Paneer Irani Tikka'
		    		},
		    		{
		    			"subItemId":230,
		    			"subItemName":'Paneer Lahsuni Tikka'
		    		}]

		    	},
		    	{
		    		"itemId":23,
		    		"itemName":"Indian Snacks",
		    		"subItems":[{
		    			"subItemId":231,
		    			"subItemName":'Veg. Manchurian (Dry)'
		    		},
		    		{
		    			"subItemId":232,
		    			"subItemName":'Mango'
		    		},
		    		{
		    			"subItemId":233,
		    			"subItemName":'Veg. Spring Roll'
		    		},
		    		{
		    			"subItemId":234,
		    			"subItemName":'Mushroom Chilli (Dry)'
		    		},
		    		{
		    			"subItemId":235,
		    			"subItemName":'Potli Samosa'
		    		},
		    		{
		    			"subItemId":236,
		    			"subItemName":'Mushroom Tikka'
		    		},
		    		{
		    			"subItemId":237,
		    			"subItemName":'Gobhi Tikka'
		    		},
		    		{
		    			"subItemId":238,
		    			"subItemName":'vegetable Salt & Pepper'
		    		},
		    		{
		    			"subItemId":239,
		    			"subItemName":'Veg. Seikh Kabab'
		    		},
		    		{
		    			"subItemId":240,
		    			"subItemName":'Honey Chilli Potatoes'
		    		},
		    		{
		    			"subItemId":241,
		    			"subItemName":'Sweet Corn Role'
		    		},
		    		{
		    			"subItemId":242,
		    			"subItemName":'Banarasi Aloo'
		    		},
		    		{
		    			"subItemId":243,
		    			"subItemName":'Aloo Pudina Tikki'
		    		},
		    		{
		    			"subItemId":244,
		    			"subItemName":'Tandoori Stuffed Tomato'
		    		},
		    		{
		    			"subItemId":245,
		    			"subItemName":'Tandoori Stuffed Capsicum'
		    		},
		    		{
		    			"subItemId":246,
		    			"subItemName":'Haryali Veg. Kabab'
		    		},
		    		{
		    			"subItemId":247,
		    			"subItemName":'(SERVED WITH DIFFERENT TYPE OF SAUCES like hot Garlic Sauce, Tartar Sauce, Mint Sauce & Tomato Sauce Etc. )'
		    		}]
		    	}]
	    },
	    {	
	    	"cid":3,
   	  		"cname":"Chat Stalls",
   	  		"items":
		    	[{
		    		"itemId":31,
		    		"itemName":"Paneer Snacks",
		    		"subItems":[{
		    			"subItemId":311,
		    			"subItemName":'Aloo Tikki'
		    		},
		    		{
		    			"subItemId":312,
		    			"subItemName":'Papri Chat'
		    		},
		    		{
		    			"subItemId":313,
		    			"subItemName":'Gol Gappa'
		    		},
		    		{
		    			"subItemId":314,
		    			"subItemName":'Mezbaan Special Chat'
		    		},
		    		{
		    			"subItemId":315,
		    			"subItemName":'Mutter Kulcha'
		    		},
		    		{
		    			"subItemId":316,
		    			"subItemName":'Paw Bhaji'
		    		},
		    		{
		    			"subItemId":317,
		    			"subItemName":'Moong Dal Chilla'
		    		},
		    		{
		    			"subItemId":318,
		    			"subItemName":'Dry Fruit Chat'
		    		},
		    		{
		    			"subItemId":319,
		    			"subItemName":'Laccha Tokri'
		    		},
		    		{
		    			"subItemId":320,
		    			"subItemName":'Raj Kachori'
		    		},
		    		{
		    			"subItemId":32-1,
		    			"subItemName":'Aloo Mutter Chat'
		    		},
		    		{
		    			"subItemId":32-2,
		    			"subItemName":'Dry Fruit Chat'
		    		}]

		    	},
		    	{
		    		"itemId":32,
		    		"itemName":"South Indian & Continental",
		    		"subItems":[{
		    			"subItemId":321,
		    			"subItemName":'Masala Dosa'
		    		},
		    		{
		    			"subItemId":322,
		    			"subItemName":'Vada'
		    		},
		    		{
		    			"subItemId":323,
		    			"subItemName":'Idly'
		    		},
		    		{
		    			"subItemId":324,
		    			"subItemName":'Mix Veg. Uttapam'
		    		},
		    		{
		    			"subItemId":325,
		    			"subItemName":'Mezbaan Special Veg. Pizza'
		    		},
		    		{
		    			"subItemId":326,
		    			"subItemName":'Mushroom Pizza'
		    		}]
		    	},
		    	{
		    		"itemId":33,
		    		"itemName":"Italian",
		    		"subItems":[{
		    			"subItemId":331,
		    			"subItemName":'Pasta (Live)'
		    		}]
		    	}]
    	},
    	{	
	    	"cid":4,
   	  		"cname":"Fruit Stalls",
   	  		"items":
		    	[{
		    		"itemId":41,
		    		"itemName":"Indian Fruits (Any-4)",
		    		"subItems":[{
		    			"subItemId":411,
		    			"subItemName":'Water Melon'
		    		},
		    		{
		    			"subItemId":412,
		    			"subItemName":'Papaya'
		    		},
		    		{
		    			"subItemId":413,
		    			"subItemName":'Rani Pineapple'
		    		},
		    		{
		    			"subItemId":414,
		    			"subItemName":'Melon'
		    		},
		    		{
		    			"subItemId":415,
		    			"subItemName":'Black Grapes'
		    		},
		    		{
		    			"subItemId":416,
		    			"subItemName":'Mango'
		    		},
		    		{
		    			"subItemId":417,
		    			"subItemName":'Apple'
		    		},
		    		{
		    			"subItemId":418,
		    			"subItemName":'Chicku'
		    		},
		    		{
		    			"subItemId":419,
		    			"subItemName":'Orange'
		    		}]

		    	},
		    	{
		    		"itemId":42,
		    		"itemName":"Imported Fruits (Any-4)",
		    		"subItems":[{
		    			"subItemId":421,
		    			"subItemName":'Sharda'
		    		},
		    		{
		    			"subItemId":422,
		    			"subItemName":'Sweet Tamarian'
		    		},
		    		{
		    			"subItemId":423,
		    			"subItemName":'Kiwi'
		    		},
		    		{
		    			"subItemId":424,
		    			"subItemName":'Australian Grapes'
		    		},
		    		{
		    			"subItemId":425,
		    			"subItemName":'Orange'
		    		},
		    		{
		    			"subItemId":426,
		    			"subItemName":'Dragon'
		    		},
		    		{
		    			"subItemId":427,
		    			"subItemName":'Australian Plum'
		    		},
		    		{
		    			"subItemId":428,
		    			"subItemName":'Asian Pear'
		    		},
		    		{
		    			"subItemId":429,
		    			"subItemName":'Apple'
		    		},
		    		{
		    			"subItemId":430,
		    			"subItemName":'Big Guava'
		    		}]
		    	}]
    	},
    	{	
	    	"cid":5,
   	  		"cname":"Salad Bar",
   	  		"items":
		    	[{
		    		"itemId":51,
		    		"itemName":"Soup",
		    		"subItems":[{
		    			"subItemId":511,
		    			"subItemName":'Tomato Dhaniya Shorba'
		    		},
		    		{
		    			"subItemId":512,
		    			"subItemName":'Manchow'
		    		},
		    		{
		    			"subItemId":513,
		    			"subItemName":'Vegetable'
		    		},
		    		{
		    			"subItemId":514,
		    			"subItemName":'Hot & Sour'
		    		},
		    		{
		    			"subItemId":515,
		    			"subItemName":'Lemon & Coriander'
		    		},
		    		{
		    			"subItemId":516,
		    			"subItemName":'Veg. Sweet Corn'
		    		}]

		    	},
		    	{
		    		"itemId":52,
		    		"itemName":"Salad",
		    		"subItems":[{
		    			"subItemId":521,
		    			"subItemName":'Fresh Green'
		    		},
		    		{
		    			"subItemId":522,
		    			"subItemName":'Russian'
		    		},
		    		{
		    			"subItemId":523,
		    			"subItemName":'Aloo Chana Chaat'
		    		},
		    		{
		    			"subItemId":524,
		    			"subItemName":'Waldorf'
		    		},
		    		{
		    			"subItemId":525,
		    			"subItemName":'Cheese & Pineapple'
		    		},
		    		{
		    			"subItemId":526,
		    			"subItemName":'Corn & Mushroom'
		    		},
		    		{
		    			"subItemId":527,
		    			"subItemName":'Fruit'
		    		},
		    		{
		    			"subItemId":528,
		    			"subItemName":'Mushroom Capsicum'
		    		},
		    		{
		    			"subItemId":529,
		    			"subItemName":'Laccha Onions'
		    		},
		    		{
		    			"subItemId":530,
		    			"subItemName":'Vinager Onions'
		    		},
		    		{
		    			"subItemId":53-1,
		    			"subItemName":'Kachumber'
		    		},
		    		{
		    			"subItemId":53-2,
		    			"subItemName":'Beans Sprout'
		    		}]
		    	},
		    	{
		    		"itemId":53,
		    		"itemName":"Pappad & Achaar",
		    		"subItems":[{
		    			"subItemId":531,
		    			"subItemName":'Rice Pappad'
		    		},
		    		{
		    			"subItemId":532,
		    			"subItemName":'Mirchi Pappad'
		    		},
		    		{
		    			"subItemId":533,
		    			"subItemName":'Aam Ka Zaika'
		    		},
		    		{
		    			"subItemId":534,
		    			"subItemName":'Neembu Garam Masala'
		    		},
		    		{
		    			"subItemId":535,
		    			"subItemName":'Banarasi Lal Mirch Bharwan'
		    		},
		    		{
		    			"subItemId":536,
		    			"subItemName":'Lazeez Adrakh Laccha'
		    		}]
		    	},
		    	{
		    		"itemId":54,
		    		"itemName":"Yogurt",
		    		"subItems":[{
		    			"subItemId":541,
		    			"subItemName":'Dahi Bhalla with Chutney'
		    		},
		    		{
		    			"subItemId":542,
		    			"subItemName":'Mix Veg. Raita'
		    		},
		    		{
		    			"subItemId":543,
		    			"subItemName":'Pineapple Raita'
		    		},
		    		{
		    			"subItemId":544,
		    			"subItemName":'Aloo Raita'
		    		},
		    		{
		    			"subItemId":545,
		    			"subItemName":'Dahi Vada'
		    		},
		    		{
		    			"subItemId":546,
		    			"subItemName":'Bundi Raita'
		    		},
		    		{
		    			"subItemId":547,
		    			"subItemName":'Pudina Raita'
		    		},
		    		{
		    			"subItemId":548,
		    			"subItemName":'Plain Raita'
		    		}]
		    	}]
    	},
    	{	
	    	"cid":6,
   	  		"cname":"Main Course",
   	  		"items":
		    	[{
		    		"itemId":61,
		    		"itemName":"Indian",
		    		"subItems":[{
		    			"subItemId":611,
		    			"subItemName":'Kadhai Paneer'
		    		},
		    		{
		    			"subItemId":612,
		    			"subItemName":'Paneer Lababdar'
		    		},
		    		{
		    			"subItemId":613,
		    			"subItemName":'Paneer / Mushroom Do Pyaza'
		    		},
		    		{
		    			"subItemId":614,
		    			"subItemName":'Shahi Paneer'
		    		},
		    		{
		    			"subItemId":615,
		    			"subItemName":'Dal Makhani'
		    		},
		    		{
		    			"subItemId":616,
		    			"subItemName":'Mushroom Masala'
		    		},
		    		{
		    			"subItemId":617,
		    			"subItemName":'Mutter Mushroom'
		    		},
		    		{
		    			"subItemId":618,
		    			"subItemName":'Aloo Zeera'
		    		},
		    		{
		    			"subItemId":619,
		    			"subItemName":'Chana Masala'
		    		},
		    		{
		    			"subItemId":620,
		    			"subItemName":'Champ Masala'
		    		},
		    		{
		    			"subItemId":62-1,
		    			"subItemName":'Mutter Methi Malai'
		    		},
		    		{
		    			"subItemId":62-2,
		    			"subItemName":'Malai Kofta'
		    		},
		    		{
		    			"subItemId":62-3,
		    			"subItemName":'Vegetable Jalfrezi'
		    		},
		    		{
		    			"subItemId":62-4,
		    			"subItemName":'Palak Paneer Kofta'
		    		},
		    		{
		    			"subItemId":62-5,
		    			"subItemName":'Kashmiri Dum Aloo'
		    		},
		    		{
		    			"subItemId":62-6,
		    			"subItemName":'Kadi Pakora'
		    		},{
		    			"subItemId":62-7,
		    			"subItemName":'Rajma Rasilla'
		    		},
		    		{
		    			"subItemId":62-8,
		    			"subItemName":'Yellow Dal Tadka'
		    		},
		    		{
		    			"subItemId":62-9,
		    			"subItemName":'Mix. Vegetables'
		    		},
		    		{
		    			"subItemId":63-0,
		    			"subItemName":'Gobhi Masala'
		    		},
		    		{
		    			"subItemId":63-1,
		    			"subItemName":'Navratna Korma'
		    		},
		    		{
		    			"subItemId":63-2,
		    			"subItemName":'Deewani Handi'
		    		}]

		    	},
		    	{
		    		"itemId":62,
		    		"itemName":"Chinese",
		    		"subItems":[{
		    			"subItemId":621,
		    			"subItemName":'Veg. Manchurian (Gravy)'
		    		},
		    		{
		    			"subItemId":622,
		    			"subItemName":'Paneer Manchurian (Gravy)'
		    		},
		    		{
		    			"subItemId":623,
		    			"subItemName":'Veg. Hakka Noodles'
		    		},
		    		{
		    			"subItemId":624,
		    			"subItemName":'Veg. Fried Rice'
		    		}]
		    	},
		    	{
		    		"itemId":63,
		    		"itemName":"Rice & Pulao",
		    		"subItems":[{
		    			"subItemId":631,
		    			"subItemName":'Jeera Rice'
		    		},
		    		{
		    			"subItemId":632,
		    			"subItemName":'Vegetable Biryani'
		    		},
		    		{
		    			"subItemId":633,
		    			"subItemName":'Steam Rice'
		    		},
		    		{
		    			"subItemId":634,
		    			"subItemName":'Kashmiri Pulao'
		    		},
		    		{
		    			"subItemId":635,
		    			"subItemName":'Navratna Pulao'
		    		},
		    		{
		    			"subItemId":636,
		    			"subItemName":'Peas Pulao'
		    		},
		    		{
		    			"subItemId":637,
		    			"subItemName":'Vegetable Pulao'
		    		},
		    		{
		    			"subItemId":638,
		    			"subItemName":'Dry Fruit Pulao'
		    		}]
		    	}]
    	},
    	{	
	    	"cid":7,
   	  		"cname":"Multi Cuisine",
   	  		"items":
		    	[{
		    		"itemId":71,
		    		"itemName":"Rajasthani",
		    		"subItems":[{
		    			"subItemId":711,
		    			"subItemName":'Dal Bati Churma'
		    		},
		    		{
		    			"subItemId":712,
		    			"subItemName":'Bajra Roti with Lusani Chutney'
		    		}]

		    	},
		    	{
		    		"itemId":72,
		    		"itemName":"Amritsari",
		    		"subItems":[{
		    			"subItemId":721,
		    			"subItemName":'Amritsari Stuffed Kulcha'
		    		},
		    		{
		    			"subItemId":722,
		    			"subItemName":'Channa Amritsari Style'
		    		},
		    		{
		    			"subItemId":723,
		    			"subItemName":'Special Chutney'
		    		}]
		    	},
		    	{
		    		"itemId":73,
		    		"itemName":"Punjabi (Seasonal)",
		    		"subItems":[{
		    			"subItemId":731,
		    			"subItemName":'Kadi Pakora'
		    		},
		    		{
		    			"subItemId":732,
		    			"subItemName":'Sarson Ka Saag'
		    		},
		    		{
		    			"subItemId":733,
		    			"subItemName":'Makke Ki Roti'
		    		},
		    		{
		    			"subItemId":734,
		    			"subItemName":'White Butter & Gur'
		    		}]
		    	},
		    	{
		    		"itemId":74,
		    		"itemName":"Tandoor Se ....",
		    		"subItems":[{
		    			"subItemId":741,
		    			"subItemName":'Tandoori Roti'
		    		},
		    		{
		    			"subItemId":742,
		    			"subItemName":'Missi Roti'
		    		},
		    		{
		    			"subItemId":743,
		    			"subItemName":'Lachha Parantha'
		    		},
		    		{
		    			"subItemId":744,
		    			"subItemName":'Lal Mirchi Parantha'
		    		},
		    		{
		    			"subItemId":745,
		    			"subItemName":'Baby Naan'
		    		},
		    		{
		    			"subItemId":746,
		    			"subItemName":'Butter Naan'
		    		},
		    		{
		    			"subItemId":747,
		    			"subItemName":'Onion Parantha'
		    		},
		    		{
		    			"subItemId":748,
		    			"subItemName":'Kandhari Naan'
		    		},
		    		{
		    			"subItemId":749,
		    			"subItemName":'Hari Mirchi Roti'
		    		},
		    		{
		    			"subItemId":750,
		    			"subItemName":'Pudina Parantha'
		    		}]
		    	}]
    	},
    	{	
	    	"cid":8,
   	  		"cname":"Desserts",
   	  		"items":
		    	[{
		    		"itemId":81,
		    		"itemName":"Sweet Dishes",
		    		"subItems":[{
		    			"subItemId":811,
		    			"subItemName":'Jalebi (Desi Ghee)'
		    		},
		    		{
		    			"subItemId":812,
		    			"subItemName":'Gulab Jamun / Rasgulla'
		    		},
		    		{
		    			"subItemId":813,
		    			"subItemName":'Malpura Rabri'
		    		},
		    		{
		    			"subItemId":814,
		    			"subItemName":'Gajar Ka Halwa (Seasonal)'
		    		},
		    		{
		    			"subItemId":815,
		    			"subItemName":'Moong Dal Halwa'
		    		},
		    		{
		    			"subItemId":816,
		    			"subItemName":'Kesari Kheer'
		    		},
		    		{
		    			"subItemId":817,
		    			"subItemName":'Ras Malai'
		    		},
		    		{
		    			"subItemId":818,
		    			"subItemName":'Sahi Tukda'
		    		},
		    		{
		    			"subItemId":819,
		    			"subItemName":'Fruit Cream'
		    		},
		    		{
		    			"subItemId":820,
		    			"subItemName":'Raj Bhog'
		    		}]

		    	},
		    	{
		    		"itemId":82,
		    		"itemName":"Ice Cream",
		    		"subItems":[{
		    			"subItemId":821,
		    			"subItemName":'Vanilla'
		    		},
		    		{
		    			"subItemId":822,
		    			"subItemName":'Butter Scotch'
		    		},
		    		{
		    			"subItemId":823,
		    			"subItemName":'Strawberry'
		    		},
		    		{
		    			"subItemId":824,
		    			"subItemName":'Chocolate'
		    		}]
		    	},
		    	{
		    		"itemId":83,
		    		"itemName":"During Pheras...",
		    		"subItems":[{
		    			"subItemId":831,
		    			"subItemName":'Mineral Water Bottle'
		    		},
		    		{
		    			"subItemId":832,
		    			"subItemName":'Kaju Burfi'
		    		},
		    		{
		    			"subItemId":833,
		    			"subItemName":'Tea / Coffee'
		    		},
		    		{
		    			"subItemId":834,
		    			"subItemName":'Roasted Kaju'
		    		},
		    		{
		    			"subItemId":835,
		    			"subItemName":'Cookies'
		    		},
		    		{
		    			"subItemId":836,
		    			"subItemName":'Chips'
		    		}]
		    	}]
		    }
	];
   	
});